import * as React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

const dataSelector = [
    { label: 'Data 1', value:1 },
    { label: 'Data 2', value:2 },
  ];

const TimeSelector = () => {

    return (
        <Autocomplete
        disablePortal
        id="combo-box-demo"
        options={dataSelector}
        renderInput={(params) => <TextField {...params} label="Rentang 5 hari" />}
      />
    )
}

export default TimeSelector;
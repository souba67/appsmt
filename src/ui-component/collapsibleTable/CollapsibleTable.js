import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { makeStyles } from "@material-ui/core/styles";


function ccyFormat(num) {
    var sign = num.toString().charAt(0);
    let reverse = num.toString().split('').reverse().join(''),
	ribuan 	= reverse.match(/\d{1,3}/g);
	ribuan	= ribuan.join('.').split('').reverse().join('');

    if (sign == '-')
    {
        ribuan = `-${ribuan}`;
    }

    return `Rp${ribuan},00.`;
  }

const useStyles = makeStyles((theme) => ({
    root: {
    //   background: (rows) => {
    //     if (rows.selisih === 30) {
    //       return "blue";
    //     } else if (rows.selisih >= 31 && rows.selisih <= 59) {
    //       return "orange";
    //     }
    //     else {
    //       return "green";
    //     }
      },
    warna1: {
        background:'red'
    },
    warna2: {
        background:'orange'
    },
    warna3: {
        background:'green'
    },
  }));


function createData(time, pentol, rekeningKoran, selisih ) {
  return { time, pentol, rekeningKoran, selisih };
}


export default function CollapsibleTable( {dataBank} ) {
    const [dataBank1, setDataBank1] = React.useState(dataBank);
    const classes = useStyles();
    const dataTable = [
      createData('03-01-2021', "500.000.000", "0", 0),
      // createData('04-01-2021', "592.000.000", "500.000.000", 92000000),
      // createData('05-01-2021', "650.000.000", "592.000.000", 58000000),
      // createData('06-01-2021', "720.000.000", "650.000.000", 70000000),
      // createData('07-01-2021', "798.000.000", "720.000.000", 78000000),
    ];
    console.log('joko')
    console.log('asukoe '+ dataBank)
  return (
    <TableContainer component={Paper}>
      <Table aria-label="Tabel Pendapatan" style={{width:"50%"}}>
        <TableHead>
          <TableRow>
            <TableCell>Tanggal</TableCell>
            <TableCell align="right">Pentol</TableCell>
            <TableCell align="right">RK</TableCell>
            <TableCell align="right">Selisih</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {dataBank.map((row) => (
            <TableRow
              key={row.time}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row" size="small">
                {row.time}
              </TableCell>
              <TableCell align="right" size="small">{ccyFormat(row.pentol)}</TableCell>
              <TableCell align="right" size="small">{ccyFormat(row.rekeningKoran)}</TableCell>
              <TableCell align="right" size="small" className={(row.selisih > 0 && row.selisih < 1000000) ? classes.warna2 : row.selisih > 1000000 ?  classes.warna1 : classes.warna3}>{ccyFormat(row.selisih)}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

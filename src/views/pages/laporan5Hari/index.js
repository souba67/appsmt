// React
import React, { PureComponent } from 'react';

//redux
import { useSelector, useDispatch, Provider } from "react-redux";

//get data
import { getData } from "../../../redux/actions/fiveDaysActions";

// material-ui
import { Typography, Grid } from '@mui/material';

// project imports
import MainCard from 'ui-component/cards/MainCard';
import SubCard from 'ui-component/cards/SubCard';
import Example from 'ui-component/piechart/PieCHart';
import TimeSelector from 'ui-component/timeSelector/TimeSelector';
import CollapsibleTable from 'ui-component/collapsibleTable/CollapsibleTable';
import TotalIncomeLightCard from '../../dashboard/Default/TotalIncomeLightCard';
import { gridSpacing } from 'store/constant';


// styled components
import styled from "styled-components";


// ==============================|| SAMPLE PAGE ||============================== //

// Styled Components
const ChartWrapper = styled.div`
    width: 100%;
    display: inline;
    height: 250px;
    justify-content: right;
    float: right;
`;
const CardWrapper = styled.div`
    width: 100%;
    display: inline;
    height: 100%;
    justify-content: right;
    float: right;
    /* padding-right: 200px; */
`;
const TableWrapper = styled.div`
    width: 100%;
    display: flex;
    height: 400px;
    justify-content: right;
    /* padding-left: 400px; */
    float: right;
`;

const Laporan5Hari = ({ history }) => {

    //state Management
    const state = useSelector(state => state.fiveDays)
    const [num, setNum] = React.useState(15);
    const dispatch  = useDispatch();
    const [dataku, setDataku] = React.useState([]);

    //fetch data
    const fetch5DaysData = (time) => {
        dispatch(getData({
          time: time, 
          dataLimaHari: num                                                                               
        }))
        console.log(`Hasilku: ${state.data}`)
        console.log(`Hasil DataBCA: ${state.data.dataBCA}`)
        setDataku(state.data.dataBCA)
      };

    //   React.useEffect( () => {
    //     if (state.data.dataBCA.length>0){
    //         setDataku(state.data.dataBCA);
    //     }
       
    //   },[state.data.dataBCA]) 
    // //   console.log('dataku adalah:'+ dataku[0].pentol)
    return (
    <MainCard title="Laporan 5 Hari Terakhir">
        <Typography>{state.data.updatedAt}</Typography>
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <SubCard title="Pie Chart 5 Hari Terakhir">
                    <Grid container spacing={gridSpacing}>
                        <Grid item lg={7} md={10} sm={6} xs={12}>
                             <CardWrapper>
                                <TotalIncomeLightCard value={state.data.dataCard}></TotalIncomeLightCard>
                            </CardWrapper>
                        </Grid>
                        <Grid item lg={5} md={2} sm={6} xs={12}>
                            <ChartWrapper>
                                <Example data={state.data.dataPiechart}></Example>
                            </ChartWrapper>
                        </Grid>
                    </Grid>
                </SubCard>
            </Grid>
            <Grid item xs={12}>
                <SubCard title="Penghasilan 5 Hari Terakhir">
                    <Grid container spacing={gridSpacing}>
                        <Grid item lg={4} md={6} sm={6} xs={12}>
                            <CardWrapper>
                                <CollapsibleTable dataBank={state.data.dataBCA}></CollapsibleTable>
                            </CardWrapper>
                        </Grid>
                        <Grid item lg={4} md={6} sm={6} xs={12}>
                            <CardWrapper>
                                <CollapsibleTable dataBank={state.data.dataBRI}></CollapsibleTable>
                            </CardWrapper>
                        </Grid>
                        <Grid item lg={4} md={6} sm={6} xs={12}>
                            <CardWrapper>
                                {/* <CollapsibleTable></CollapsibleTable> */}
                            </CardWrapper>
                        </Grid>
                        <Grid item lg={4} md={6} sm={6} xs={12}>
                            <CardWrapper>
                                {/* <CollapsibleTable></CollapsibleTable> */}
                            </CardWrapper>
                        </Grid>
                        <Grid item lg={4} md={6} sm={6} xs={12}>
                            <CardWrapper>
                                {/* <CollapsibleTable></CollapsibleTable> */}
                            </CardWrapper>
                        </Grid>
                        <Grid item lg={4} md={6} sm={6} xs={12}>
                            <CardWrapper>
                                <TimeSelector></TimeSelector>
                                <button onClick={() => fetch5DaysData(1)}>post1</button>
                            </CardWrapper>
                        </Grid>
                    </Grid>
                </SubCard>
            </Grid>
        </Grid>
    </MainCard>
);
};
export default Laporan5Hari;
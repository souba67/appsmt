import { combineReducers } from 'redux';

// reducer import
import customizationReducer from './customizationReducer';
import fiveDaysReducers from '../redux/reducers/fiveDaysReducers'

// ==============================|| COMBINE REDUCER ||============================== //

const reducer = combineReducers({
    customization: customizationReducer,
    fiveDays: fiveDaysReducers
});

export default reducer;

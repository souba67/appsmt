import { createStore, applyMiddleware } from 'redux';
import reducer from './reducer';
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

// ==============================|| REDUX - MAIN STORE ||============================== //

const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware(thunk))
    );
const persister = 'Free';

export { store, persister };

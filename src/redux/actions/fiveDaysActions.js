import axios from "axios";

export const getData = ({ time, dataLimaHari }) => async dispatch => {
    try {
        dispatch({
            type: "AWAITING_FIVEDAYS",
        })
        const dataku = await axios.get(`http://localhost:3002/data/${time}`).then(res => res.data)

        const data = [];

        // for(let i = 0; i < response.data.length; i++){
        //     data = response

        // if ( i === (dataLimaHari -1)) {
        //     break;
        // }
    // }    
    dispatch({
        type:"SUCCESS-FIVEDAYS",
        payload: {
            data: dataku
            }
        })
    // console.log(`Isi API: ${dataku.data}`)
    } catch (e) {
        dispatch({
            type: "REJECTED-FIVEDAYS",
        })
    }
}
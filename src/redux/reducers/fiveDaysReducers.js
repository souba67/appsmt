const initialState = {
    loading: false,
    data: {
        id:0,
        updatedAt: "Belum Ada Data",
        dataCard: "Belum Ada Data",
        dataPiechart: [],
        dataBCA: [],
        dataBNI: [],
        dataBRI: [],
        dataDKI: [],
        dataMandiri: [],
    }
};

const fiveDaysReducers = (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case "AWAITING_FIVEDAYS":
            return {
                ...state,
                loading: true,
            }
        case "REJECTED-FIVEDAYS":
            return {
                ...state,
                loading: false,
            }
        case "SUCCESS-FIVEDAYS":
            // let arrPiechart = [];
            // let arrDataBCA = [];
            // let arrDataBNI = [];
            // let arrDataBRI = [];
            // let arrDataDKI = [];
            // let arrDataMandiri = [];

            // payload.data.dataPiechart.forEach((item, index) => {
            //     arrPiechart.push({
            //         name:  payload.data.dataPiechart.name, 
            //         value:  payload.data.dataPiechart.value
            //     })
            // })
            // payload.data.dataBCA.forEach((item, index) => {
            //     arrDataBCA.push({
            //         time: "05-01-21", 
            //         pentol: 10000000, 
            //         rekeningKoran: 50000000, 
            //         selisih:5000000
            //     })
            // })
            // payload.data.dataBNI.forEach((item, index) => {
            //     arrDataBNI.push({
            //         time: "05-01-21", 
            //         pentol: 10000000, 
            //         rekeningKoran: 50000000, 
            //         selisih:5000000
            //     })
            // })
            // payload.data.dataBRI.forEach((item, index) => {
            //     arrDataBRI.push({
            //         time: "05-01-21", 
            //         pentol: 10000000, 
            //         rekeningKoran: 50000000, 
            //         selisih:5000000
            //     })
            // })
            // payload.data.dataDKI.forEach((item, index) => {
            //     arrDataDKI.push({
            //         time: "05-01-21", 
            //         pentol: 10000000, 
            //         rekeningKoran: 50000000, 
            //         selisih:5000000
            //     })
            // })
            // payload.data.dataMandiri.forEach((item, index) => {
            //     arrDataMandiri.push({
            //         time: "05-01-21", 
            //         pentol: 10000000, 
            //         rekeningKoran: 50000000, 
            //         selisih:5000000
            //     })
            // })
            return {
                ...state,
                loading: false,
                data: {
                    id: payload.data.id,
                    updatedAt: payload.data.updatedAt,
                    dataCard: payload.data.dataCard,
                    dataPiechart: payload.data.dataPiechart,
                    dataBCA: payload.data.dataBCA,     
                    dataBNI: payload.data.dataBNI,     
                    dataBRI: payload.data.dataBRI,     
                    dataDKI: payload.data.dataDKI,     
                    dataMandiri: payload.dataMandiri  
                },
        }
        default:
            return state;
    }
    return state;
}

export default fiveDaysReducers;
import { combineReducers } from "redux";
import fiveDaysReducers from "./fiveDaysReducers";

const rootReducer = combineReducers({
  fiveDays: fiveDaysReducers
})

export default rootReducer;